/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
   <>
    <View style={styles.container}>
      <View style={styles.card}>
        <View style={styles.header}>
          <Text style={styles.nameText}>React Native School</Text>
          <Text style={styles.followText}>follow</Text>
        </View>
        <Image
          style={styles.image}
          resizeMode = "cover"
          source={{
            uri : "https://images.pexels.com/photos/3225517/pexels-photo-3225517.jpeg?cs=srgb&dl=pexels-michael-block-3225517.jpg",
        }}
        />
        <View style={styles.footer}>
          <Text>
            <Text style={styles.nameText}>React Native School {'\t'}</Text>
            This has been a tutorial on how to build a layout with flexbox. I hope you enjoyed !!!
          </Text>
        </View>
      </View>
    </View>
   </>
  );
};

const screen = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor : '#7CA1B4',
    alignItems: 'center',
    justifyContent: 'center'
  },
  card: {
    backgroundColor: '#fff',
    width: screen.width * 0.8,

  },
  image : {
    height: screen.width * 0.8,
  },
  nameText : {
    fontWeight: 'bold',
    color: '#20232a',
  },
  followText : {
    fontWeight: 'bold',
    color : '#0095f6'
  },
  header: {
    flexDirection: 'row',
    justifyContent : 'space-between',
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  footer :{
    paddingHorizontal: 15,
    paddingVertical: 10
  }
});

export default App;
